/**
 * @file st7735r.c
 * @brief Main source file for the ST7735R LCD chip.
 * @details pinkeee @ j.t0dd@protonmail.com
 * @note    See README for contributing guides and info.
 * @copyright See License file.
 */

#include "st7735r.h"

/**
  * @brief Set SPI1 CS pin
  * @param Void
  * @return Void N/A
*/
void ST7735R_CS_High(void)
{
  HAL_GPIO_WritePin(SPI1_CS_GPIO_Port, SPI1_CS_Pin, GPIO_PIN_SET);
}

/**
  * @brief Bring low SPI1 CS pin
  * @param Void
  * @return Void N/A
*/
void ST7735R_CS_Low(void)
{
  HAL_GPIO_WritePin(SPI1_CS_GPIO_Port, SPI1_CS_Pin, GPIO_PIN_RESET);
}

/**
  * @brief Write command on device
  * @param cmd what command to write to
  * @return either ST7735R_RET_OK or ST7735R_RET_FAIL
  */
ST7735R_Return ST7735R_Write_Command(struct ST7735R *self, uint8_t cmd)
{
  HAL_GPIO_WritePin(A0_GPIO_Port, A0_Pin, GPIO_PIN_RESET);

  ST7735R_CS_Low();
  if (HAL_SPI_Transmit(self->SPI_Handler, &cmd, sizeof(cmd), ST7735R_SPI_TIMEOUT) != HAL_OK)
  {
    ST7735R_CS_High();
    return ST7735R_RET_FAIL;
  }
  ST7735R_CS_High();
  return ST7735R_RET_OK;
}

/**
  * @brief Write data to RAM on device
  * @param data what command to write to
  * @param size size of data buffer
  * @return either ST7735R_RET_OK or ST7735R_RET_FAIL
  */
ST7735R_Return ST7735R_Write_Data(struct ST7735R *self, uint8_t *data, uint8_t size)
{
  HAL_GPIO_WritePin(A0_GPIO_Port, A0_Pin, GPIO_PIN_SET);

  ST7735R_CS_Low();
  if (HAL_SPI_Transmit(self->SPI_Handler, data, size, ST7735R_SPI_TIMEOUT) != HAL_OK)
  {
    ST7735R_CS_High();
    return ST7735R_RET_FAIL;
  }
  ST7735R_CS_High();
  return ST7735R_RET_OK;
}

/**
  * @brief Turn backlight off
  * @return either ST7735R_RET_OK or ST7735R_RET_FAIL
  */
ST7735R_Return ST7735R_Backlight_Off(struct ST7735R *self)
{
  uint8_t tx[2] = {0};
  uint8_t rx[10] = {0};

  tx[0] = ST7735R_DISPOFF_CMD;

  ST7735R_CS_Low();
  HAL_GPIO_WritePin(A0_GPIO_Port, A0_Pin, GPIO_PIN_RESET);

  HAL_SPI_Transmit(self->SPI_Handler, tx, sizeof(tx), 100);
  //HAL_SPI_Receive(self->SPI_Handler, rx, 4, 100);
  ST7735R_CS_High();

  return ST7735R_RET_OK;
}