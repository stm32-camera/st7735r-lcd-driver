/**
 * @file st7735r.h
 * @brief Main header file for the ST7735R LCD chip.
 *
 * @details pinkeee @ j.t0dd@protonmail.com
 * @note    See README for contributing guides and info.
 * @copyright See License file.
 */

#ifndef ST7735R_H
#define ST7735R_H

#include "main.h"

#define ST7735R_RET_OK 0
#define ST7735R_RET_FAIL 0

#define ST7735R_SPI_TIMEOUT 100

/* _Command list_ */

/**
  * @brief NOP command
  * @details This command is empty command.
  * @return 0x00
*/
static uint8_t ST7735R_NOP_CMD = 0x00;

/**
  * @brief SWRESET command
  * @details Software reset
  * @return 0x01
*/
static uint8_t ST7735R_SWRESET_CMD = 0x01;

/**
  * @brief RDDID command
  * @details Read Display ID
  * @return 0x04
*/
static uint8_t ST7735R_RDDID_CMD = 0x04;

/**
  * @brief RDDST command
  * @details Read Display Status
  * @return 0x09
*/
static uint8_t ST7735R_RDDST_CMD = 0x09;

/**
  * @brief RDDPM command
  * @details Read Display Power
  * @return 0xA0
*/
static uint8_t ST7735R_RDDPM_CMD = 0xA0;

/**
  * @brief RDDMADCTL command
  * @details Read Display
  * @return 0xB0
*/
static uint8_t ST7735R_RDDMADCTL_CMD = 0xB0;

/**
  * @brief RDDCOLMOD command
  * @details Read Display Pixel
  * @return 0xC0
*/
static uint8_t ST7735R_RDDCOLMOD_CMD = 0xC0;

/**
  * @brief RDDIM command
  * @details Read Display Image
  * @return 0xD0
*/
static uint8_t ST7735R_RDDIM_CMD = 0xD0;

/**
  * @brief RDDSM command
  * @details Read Display Signal
  * @return 0xE0
*/
static uint8_t ST7735R_RDDSM_CMD = 0xE0;

/**
  * @brief SLPIN command
  * @details Sleep in & booster off
  * @return 0x10
*/
static uint8_t ST7735R_SLPIN_CMD = 0x10;

/**
  * @brief SLPOUT command
  * @details Sleep out & booster on
  * @return 0x11
*/
static uint8_t ST7735R_SLPOUT_CMD = 0x11;

/**
  * @brief PTLON command
  * @details Partial mode on
  * @return 0x12
*/
static uint8_t ST7735R_PTLON_CMD = 0x12;

/**
  * @brief NORON command
  * @details Partial off (Normal)
  * @return 0x13
*/
static uint8_t ST7735R_NORON_CMD = 0x13;

/**
  * @brief INVOFF command
  * @details Display inversion off
  * @return 0x20
*/
static uint8_t ST7735R_INVOFF_CMD = 0x20;

/**
  * @brief INVON command
  * @details Display inversion on
  * @return 0x21
*/
static uint8_t ST7735R_INVON_CMD = 0x21;

/**
  * @brief GAMSET command
  * @details Gamma curve select
  * @return 0x26
*/
static uint8_t ST7735R_GAMSET_CMD = 0x26;

/**
  * @brief DISPOFF command
  * @details Display off
  * @return 0x28
*/
static uint8_t ST7735R_DISPOFF_CMD = 0x28;

/**
  * @brief DISPON command
  * @details Display on
  * @return 0x29
*/
static uint8_t ST7735R_DISPON_CMD = 0x29;

/**
  * @brief CASET command
  * @details Column address set
  * @return 0x2A
*/
static uint8_t ST7735R_CASET_CMD = 0x2A;

/**
  * @brief RASET command
  * @details Row address set
  * @return 0x2B
*/
static uint8_t ST7735R_RASET_CMD = 0x2B;

/**
  * @brief RAMWR command
  * @details Memory write
  * @return 0x2C
*/
static uint8_t ST7735R_RAMWR_CMD = 0x2C;

/**
  * @brief RGBSET command
  * @details LUT for 4k,65k,262k color
  * @return 0x2D
*/
static uint8_t ST7735R_RGBSET_CMD = 0x2D;

/**
  * @brief RAMRD command
  * @details Memory read
  * @return 0x2E
*/
static uint8_t ST7735R_RAMRD_CMD = 0x2E;

/**
  * @brief PTLAR command
  * @details Partial start/end address set
  * @return 0x30
*/
static uint8_t ST7735R_PTLAR_CMD = 0x30;

/**
  * @brief TEOFF command
  * @details Tearing effect line off
  * @return 0x34
*/
static uint8_t ST7735R_TEOFF_CMD = 0x34;

/**
  * @brief TEON command
  * @details Tearing effect mode set & on
  * @return 0x35
*/
static uint8_t ST7735R_TEON_CMD = 0x35;

/**
  * @brief MADCTL command
  * @details Memory data access control
  * @return 0x36
*/
static uint8_t ST7735R_MADCTL_CMD = 0x36;

/**
  * @brief IDMOFF command
  * @details Idle mode off
  * @return 0x38
*/
static uint8_t ST7735R_IDMOFF_CMD = 0x38;

/**
  * @brief IDMON command
  * @details Idle mode on
  * @return 0x39
*/
static uint8_t ST7735R_IDMON_CMD = 0x39;

/**
  * @brief COLMOD command
  * @details Interface pixel format
  * @return 0x3A
*/
static uint8_t ST7735R_COLMOD_CMD = 0x3A;

/**
  * @brief RDID1 command
  * @details Read ID1
  * @return 0xDA
*/
static uint8_t ST7735R_RDID1_CMD = 0xDA;

/**
  * @brief RDID2 command
  * @details Read ID2
  * @return 0xDB
*/
static uint8_t ST7735R_RDID2_CMD = 0xDB;

/**
  * @brief RDID3 command
  * @details Read ID3
  * @return 0xDC
*/
static uint8_t ST7735R_RDID3_CMD = 0xDC;

/*
 * Write to the display:
 *  - Looking at the datasheet, we have have a special 'D/CX' pin that is also 
 *    confusingly called 'A0' on the LCD itself. When writing a command byte this
 *    pin needs to be 'low' or in a RESET state as it is sometimes called. When writing
 *    parameters or data to be stored in the LCD's RAM the pin must be set 'high' or in
 *    a SET state.
 *
*/

struct ST7735R
{
  /* SPI1 Handler */
  SPI_HandleTypeDef *SPI_Handler;
};

typedef int ST7735R_Return;

/**
  * @brief Set SPI1 CS pin
  * @param Void
  * @return Void N/A
*/
void ST7735R_CS_High(void);

/**
  * @brief Bring low SPI1 CS pin
  * @param Void
  * @return Void N/A
*/
void ST7735R_CS_Low(void);

/**
  * @brief Write command on device
  * @param cmd what command to write to
  * @return either ST7735R_RET_OK or ST7735R_RET_FAIL
  */
ST7735R_Return ST7735R_Write_Command(struct ST7735R *self, uint8_t cmd);

/**
  * @brief Write data to RAM on device
  * @param data what command to write to
  * @param size size of data buffer
  * @return either ST7735R_RET_OK or ST7735R_RET_FAIL
  */
ST7735R_Return ST7735R_Write_Data(struct ST7735R *self, uint8_t *data, uint8_t size);

/**
  * @brief Turn backlight off
  * @return either ST7735R_RET_OK or ST7735R_RET_FAIL
  */
ST7735R_Return ST7735R_Backlight_Off(struct ST7735R *self);

#endif /* ST7735R_H */